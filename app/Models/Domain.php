<?php

namespace App\Models;

use App\Enum\ForbidTarget;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Domain extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];

    public function forbiddenSources(): BelongsToMany
    {
        return $this
            ->belongsToMany(ForbiddenSource::class)
            ->where('target', ForbidTarget::ParticularDomains->value);
    }
}
