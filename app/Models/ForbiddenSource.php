<?php

declare(strict_types=1);

namespace App\Models;

use App\Enum\ForbiddenSource as Source;
use App\Enum\ForbidTarget;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use InvalidArgumentException;

/**
 * @property int $source
 * @property int $target
 * @property string $value
 */
class ForbiddenSource extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'source',
        'target',
        'value',
    ];

    public function domains(): BelongsToMany
    {
        return $this->belongsToMany(Domain::class);
    }

    public function setSourceAttribute($value): void
    {
        if (!Source::tryFrom($value)) {
            throw new InvalidArgumentException("Invalid source value");
        }

        $this->attributes['source'] = $value;
    }

    public function setTargetAttribute($value): void
    {
        if (!ForbidTarget::tryFrom($value)) {
            throw new InvalidArgumentException("Invalid target value");
        }

        $this->attributes['target'] = $value;
    }
}
