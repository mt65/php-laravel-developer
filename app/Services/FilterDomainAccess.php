<?php

declare(strict_types=1);

namespace App\Services;

use App\Enum\ForbiddenSource;
use App\Enum\ForbidTarget;
use App\Models\Domain;
use App\Request;

class FilterDomainAccess
{
    public function __construct(
        private readonly Request $request
    )
    {
    }

    public function isAllowed(): bool
    {
        if (!($this->request->domain() instanceof Domain)) {
            return true;
        }

        return $this->isUserAgentAllowed() && $this->isIpAllowed();
    }

    private function isUserAgentAllowed(): bool
    {
        /* TODO
         * Проверить есть ли ForbiddenSource с source = ForbiddenSource::UserAgent->value, target = ForbidTarget::AllDomains->value
         * и value совпадающим с $this->request->userAgent()
         * либо есть ли для модели домена связаная через forbidden_source_domain запись, где target = ForbidTarget::ParticularDomains->value,
         * source = ForbiddenSource::UserAgent->value и value совпадающим с $this->request->userAgent()
         */

        return true;
    }

    private function isIpAllowed(): bool
    {
        /* TODO
         * Проверить есть ли ForbiddenSource с source = ForbiddenSource::IP->value, target = ForbidTarget::AllDomains->value
         * и value совпадающим с $this->request->ip()
         * либо есть ли для модели домена связаная через forbidden_source_domain запись, где target = ForbidTarget::ParticularDomains->value,
         * source = ForbiddenSource::IP->value и value совпадающим с $this->request->ip()
         */
        return true;
    }

}