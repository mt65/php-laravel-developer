<?php

declare(strict_types=1);

namespace App\Enum;

enum ForbidTarget: int
{
    case AllDomains = 1;
    case ParticularDomains = 2;
}