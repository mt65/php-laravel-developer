<?php

declare(strict_types = 1);

namespace App\Enum;

enum ForbiddenSource: int
{
    case IP = 1;
    case UserAgent = 2;
}