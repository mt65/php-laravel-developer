<?php

namespace App\Http\Middleware;

use App\Services\FilterDomainAccess;
use Closure;
use App\Request;
use Symfony\Component\HttpFoundation\Response;

class DomainAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $accessService = new FilterDomainAccess($request);

        if ($accessService->isAllowed()) {
            return $next($request);
        }

        return response('Access denied', 403);
    }
}
