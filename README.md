# Тестовое задание

Данный документ описывает тестовое задание, которое должен выполнить кандидат на должность Middle PHP/Laravel Developer.

После выполнения задания ваши решения ожидаем ссылками на собственные репозитории

Время решения ≈ 3 часа

Это стандартный экземпляр Laravel собранный на Sail по средством https://laravel.build/

## Основная задача

В системе одно приложение используется для множества доменов (одно приложение показывает разный контент для разных доменов).
Домены также являются сущностями в приложении.

Есть бизнес задача, производить фильтрацию запросов на домены исходя из: User-Agent, IP-адреса. Также необходимо учесть что в перспективе, могут появиться дополнительные условия, например: _**запрещать доступ с определенных IP адресов только для определенных доменов**_.

Задача - написать основной код для фильтрации запросов. Реализацию сущностей или иных вспомогательных классов можно опустить при желании.

Основная цель - продемонстрировать подход к решению задач.

Плюсом будет, если напиасание тестов (**unit**) под основной код.

### Решение
В своем решении исходил из того, что требуется реализовать фильтрацию доступа к доменам на основании блэклиста с  IP адресами и User-Agent.
Добавил модель для храннеия запрещенных IP и User-Agent - ForbiddenSources. Поле target отвечает за цель блокировки, либо все домены, либо выборочно некоторые из них.
При использовании в качестве цели выборочных доменов создается запись в таблице forbidden_source_domain для связи конкретной записи из forbidden_source с одинм или несколькими доменами.
Также добавил класс, который отвечает за проверку доступа к домену на основании данных пришедшего запроса - FilterDomainAccess. 
Данный класс инстанциируется в DomainAccessMiddleware, которая применяется к каждому запросу, и там вызывается его метод осуществляющий проверку.


## Второстепенные задачи

- Разобраться почему не проходит```tests/Feature/Api/DomainUpdateTest```
- Ответ: Првоерку через DomainPolicy нужно перенести в метод контроллера и использовать там встроенный метод authorize, куда кроме действия нужно передать экземпляр домена, доступ к которому мы проверяем ($this->authorize('update', $domain).

- Описать в каких случаях ```tests/Unit/FakerTest```  будет завершаться с ошибкой

